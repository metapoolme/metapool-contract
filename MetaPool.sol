// SPDX-License-Identifier: MIT

pragma solidity 0.8.11;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

interface IHolder {
    function lockFundsFor(address _user, uint256 _amount) external;
}

contract METAPOOL is ERC20, ERC20Burnable, Ownable {
    IERC20 public busd;
    address public holder;
    address public management;

    event Sold(address indexed to, uint256 value);

    event ChangedManagement(address indexed prevManagement, address indexed newManagement);

    constructor(address _busd) ERC20("METAPOOL", "MTP") {
        _mint(address(this), 1000000 * 10**decimals());
        busd = IERC20(_busd);
        management = _msgSender();
    }

    uint256 public busdRaisedSecond = 0;
    uint256 public secondRoundCap = 0;

    struct FreezedBunch {
        uint256 amount;
        uint256 time;
        uint256 duration;
    }

    mapping(address => FreezedBunch[]) private _freezes;

    function getOwner() external view returns (address) {
        return owner();
    }

    function setManagement(address newManagement) external onlyOwner {
        address prevManagement = management;
        management = newManagement;

        emit ChangedManagement(prevManagement, newManagement);
    }

    function setHolder(address _holder) external onlyOwner {
        holder = _holder;
    }

    function _freeze(
        address to,
        uint256 amount,
        uint256 duration
    ) internal {
        _freezes[to].push(FreezedBunch(amount, block.timestamp, duration));
    }

    function freezedAmount(address owner) public view returns (uint256) {
        uint256 amount = 0;
        for (uint256 i = 0; i < _freezes[owner].length; i++) {
            if (_freezes[owner][i].time + _freezes[owner][i].duration > block.timestamp) {
                amount += _freezes[owner][i].amount;
            }
        }
        return amount;
    }

    function _unFreezeAmount(address owner, uint256 amount) internal {
        // old holds are at the beginning of the array, we start from the beginning of the array
        for (uint256 i = 0; i < _freezes[owner].length; i++) {
            if (
                0 == _freezes[owner][i].amount ||
                _freezes[owner][i].time + _freezes[owner][i].duration <= block.timestamp
            ) {
                continue;
            }

            // if the amount in the current element is more or equal the required amount,
            // we subtract the required amount and stop the cycle
            if (amount <= _freezes[owner][i].amount) {
                _freezes[owner][i].amount -= amount;
                break;
            }

            // If there are not enough tokens in the current element, we reset the current element and subtract
            // the number of tokens from the required number, the cycle continues
            if (amount > _freezes[owner][i].amount) {
                amount -= _freezes[owner][i].amount;
                _freezes[owner][i].amount = 0;
            }
        }
    }

    // check that account have necessary balance non frozen
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal override {
        super._beforeTokenTransfer(from, to, amount);
        // If the recipient is a contract holder, then unfreeze the tokens
        if (to == holder) {
            _unFreezeAmount(from, amount);
        }
        if (from != address(0) && to != holder) {
            uint256 senderRealBalance = balanceOf(from) - freezedAmount(from);
            require(senderRealBalance >= amount, "METAPOOL: not enough non-frozen tokens");
        }
    }

    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal override {
        // If the balance of the holder is replenished by the manager,
        // then the funds are not blocked
        if (to == holder && _msgSender() != management) {
            // Any other transfer to the holder's address blocks the funds for a year
            IHolder(holder).lockFundsFor(from, amount);
        }
    }

    function withdrawBnb() public onlyOwner {
        address payable _owner = payable(msg.sender);
        _owner.transfer(address(this).balance);
    }

    function withdrawBusd(address to, uint256 amount) public onlyOwner {
        busd.transfer(to, amount);
    }

    function withdrawTokens(address to, uint256 amount) public onlyOwner {
        _transfer(address(this), to, amount);
    }

    function withdrawFrozenTokens(
        address to,
        uint256 amount,
        uint256 duration
    ) public onlyOwner {
        _transfer(address(this), to, amount);
        _freeze(to, amount, duration);
    }

    function mint(address to, uint256 amount) public onlyOwner {
        require(amount > 0, "Amount too low");
        _mint(to, amount);
    }

    function setSecondCap(uint256 cap) public onlyOwner {
        secondRoundCap = cap;
    }

    function tokenSaleSecondRound(uint256 amount) external {
        require(amount > 0, "Amount too low");
        require(busdRaisedSecond < secondRoundCap, "Round ended or not yet started");
        uint256 allowance = busd.allowance(msg.sender, address(this));
        require(allowance >= amount, "You should approve BUSD spending first");
        busd.transferFrom(msg.sender, address(this), amount);

        _transfer(address(this), msg.sender, amount);
        _freeze(msg.sender, amount, 180 days);
        busdRaisedSecond += amount;
        emit Sold(msg.sender, amount);
    }
}
